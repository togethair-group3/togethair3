INSERT INTO employee(id,email,firstName,lastName,password) VALUES(1000,'ayoub.rossi@realdolmen.com','Ayoub','Rossi', 'a');
INSERT INTO employee(id,email,firstName,lastName,password) VALUES(1001,'tariq.rossi@realdolmen.com','Tariq','Rossi', 'a');
INSERT INTO employee(id,email,firstName,lastName,password) VALUES(1002,'lars.rossi@realdolmen.com','Lars','Rossi', 'a');
INSERT INTO employee(id,email,firstName,lastName,password) VALUES(1003,'ellis.rossi@realdolmen.com','Ellis','Rossi', 'a');
INSERT INTO employee(id,email,firstName,lastName,password) VALUES(1004,'alexandre.rossi@realdolmen.com','Alexandre','Rossi', 'a');
INSERT INTO employee(id,email,firstName,lastName,password) VALUES(1005,'kevin.rossi@realdolmen.com','Kevin','Rossi', 'a');

INSERT INTO airline(id, email, name, password) VALUES (1000, 'ryanair@ryanair.com', 'ryanair', 'r');
INSERT INTO airline(id, email, name, password) VALUES (1001, 'tuifly@tuifly.com', 'tuifly', 't');
INSERT INTO airline(id, email, name, password) VALUES (1002, 'vueling@vueling.com', 'vueling', 'v');
INSERT INTO airline(id, email, name, password) VALUES (1003, 'kpm@kpm.com', 'kpm', 'k');
INSERT INTO airline(id, email, name, password) VALUES (1004, 'wizzair@wizzair.com', 'wizzair', 'w');

INSERT INTO location(id, airportCode, country, globalRegion, name) VALUES (1000, 'LHR', 'United Kingdom', 'Western Europe', 'London Heathrow Airport');
INSERT INTO location(id, airportCode, country, globalRegion, name) VALUES (1001, 'CGN', 'Germany', 'Western Europe', 'Cologne Bonn Airport');
INSERT INTO location(id, airportCode, country, globalRegion, name) VALUES (1002, 'CPH', 'Denmark', 'Western Europe', 'Kopenhagen Airport');
INSERT INTO location(id, airportCode, country, globalRegion, name) VALUES (1003, 'JFK', 'United States of America', 'North Amerika', 'John F. Kennedy Airport');
INSERT INTO location(id, airportCode, country, globalRegion, name) VALUES (1004, 'TNR', 'Madagascar', 'Africa', 'Antananarivo International Airport');
INSERT INTO location(id, airportCode, country, globalRegion, name) VALUES (1005, 'HKT', 'Thailand', 'Asia', 'Phuket International Airport');
