$(document).ready(function() {
    $('select.flightTypes').on('change', function() {
        $('.returnDate').prop('disabled', !($(this).val() == 1));
    });
});