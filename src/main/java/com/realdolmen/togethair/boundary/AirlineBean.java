package com.realdolmen.togethair.boundary;

import com.realdolmen.togethair.domain.Airline;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.List;

/**
 * Created by ARSBL93 on 08/11/2018.
 */
@Named
@SessionScoped
public class AirlineBean implements Serializable {

    @PersistenceContext
    EntityManager entityManager;

    public List<Airline> findAllAirlines() {
        Query query = entityManager.createQuery("SELECT l FROM Airline l", Airline.class);
        return query.getResultList();
    }


}
