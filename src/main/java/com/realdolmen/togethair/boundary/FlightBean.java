package com.realdolmen.togethair.boundary;

import com.realdolmen.togethair.domain.Airline;
import com.realdolmen.togethair.domain.Flight;

import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.List;

@Named
@Stateful
@SessionScoped
public class FlightBean implements Serializable {

    @PersistenceContext
    EntityManager em;

    public Flight getCurrent() {
        return current;
    }

    public void setCurrent(Flight current) {
        this.current = current;
    }

    private Flight current;

    public Flight create() {
        return current = new Flight();
    }

    public void saveFlight() {
        em.persist(current);
    }

    public void removeFlight(Flight flight) {
        em.remove(flight);
    }

    public Flight findById(Long id) {
        return em.find(Flight.class, id);
    }

    public List<Flight> findAllFlights() {
        Query query = em.createQuery("SELECT l FROM Flight l", Flight.class);
        return query.getResultList();
    }



}

