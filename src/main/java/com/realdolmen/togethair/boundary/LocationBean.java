package com.realdolmen.togethair.boundary;

import com.realdolmen.togethair.domain.Location;

import javax.ejb.Stateful;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.List;

/**
 * Created by ARSBL93 on 08/11/2018.
 */
@Named
@ViewScoped
@Stateful
public class LocationBean implements Serializable{

    @PersistenceContext
    EntityManager entityManager;

    private Location currentLocation = new Location();

    public List<Location> findAllLocations() {
        Query query = entityManager.createQuery("SELECT l FROM Location l", Location.class);
        return query.getResultList();
    }

    public Location findLocationById() {
        return entityManager.find(Location.class, 1L);
    }

    public void setCurrentLocation(Location location){
        System.out.println("setCurrentLocation");
        currentLocation = location;
    }

    public Location getCurrentLocation() {
        return currentLocation;
    }

    public void updateLocation(Location location) {
        System.out.println("updateLocation");
        entityManager.merge(location);
        resetCurrentLocation();
    }

    public void deleteLocation(Long id) {
        System.out.println("deleteLocation");
        entityManager.remove(entityManager.find(Location.class, id));
        resetCurrentLocation();
    }

    public void addLocation(Location location) {
        System.out.println("addLocation");
        entityManager.persist(location);
    }

    public void resetCurrentLocation() {
        System.out.println("resetCurrentLocation");
        currentLocation = new Location();
    }

}
