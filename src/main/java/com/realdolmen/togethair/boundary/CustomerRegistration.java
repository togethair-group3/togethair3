package com.realdolmen.togethair.boundary;

import com.realdolmen.togethair.domain.Customer;

import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;

/**
 * Created by ARSBL93 on 08/11/2018.
 */
@Named
@Stateful
@SessionScoped
public class CustomerRegistration implements Serializable {

    @PersistenceContext
    private EntityManager em;

    private Customer current;

    public Customer create() {
        return current = new Customer();
    }

    public Customer getCurrent() {
        return current;
    }

    public void setCurrent(Customer current) {
        this.current = current;
    }

    public void saveCustomer() {

        em.persist(current);
    }
}
