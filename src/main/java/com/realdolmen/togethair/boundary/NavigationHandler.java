package com.realdolmen.togethair.boundary;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Created by ARSBL93 on 08/11/2018.
 */
@Named
@ViewScoped
public class NavigationHandler implements Serializable {

    public String goToRegistration() {
        return "CustomerRegistration";
    }

    public String goToLogin() {
        return "Login";
    }

    public String goToCustomerLogin() {
        return "CustomerLogin";
    }

    public String goToAirlineLogin() { return "AirlineLogin"; }

    public String goToEmployeeLogin() {
        return "EmployeeLogin";
    }

}
