package com.realdolmen.togethair.boundary;

import com.realdolmen.togethair.domain.Airline;

import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;


/**
 * Created by ARSBL93 on 08/11/2018.
 */

@Named
@Stateful
@SessionScoped
public class AirlineLoginBean extends LoginBean<Airline> {

    {
        setClassName("Airline");
        setRedirectAfterLogin("AirlineInterface");
    }
}
