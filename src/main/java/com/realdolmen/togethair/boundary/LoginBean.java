package com.realdolmen.togethair.boundary;

import com.realdolmen.togethair.domain.Airline;

import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.List;

/**
 * Created by ARSBL93 on 08/11/2018.
 */

public abstract class LoginBean<T> implements Serializable {

    private String email;
    private String password;
    private String className;
    private String redirectAfterLogin;

    private boolean loggedIn;

    @PersistenceContext
    private EntityManager entityManager;

    public String login(){
        if(!(findUserWithCredentials(email,password).isEmpty())){
            System.out.println("OK");
            loggedIn=true;
            return redirectAfterLogin;
        }else{
            System.out.println("NOT OK");
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Incorrect Username and Password",
                            "Please enter correct username and Password"));
            return "";
        }
    }

    public List<T> findUserWithCredentials(String email, String password) {
        Query query = entityManager.createQuery("SELECT a FROM "+className+" a WHERE a.email = :email and a.password = :password");
        return query.setParameter("email", email).setParameter("password", password).getResultList();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getRedirectAfterLogin() {
        return redirectAfterLogin;
    }

    public void setRedirectAfterLogin(String redirectAfterLogin) {
        this.redirectAfterLogin = redirectAfterLogin;
    }
}
