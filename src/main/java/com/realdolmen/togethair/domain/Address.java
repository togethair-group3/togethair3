package com.realdolmen.togethair.domain;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Embeddable;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Embeddable
public class Address {

    @Size(min = 2, max = 200, message ="Street requires a minimum of 2 and a maximum of 200 characters")
	private String street;

    @Size(min=1, max = 10, message ="Number requires a minimum of 1 and a maximum of 10 characters")
    private String streetNumber;

    @Size(min = 2, max = 10, message ="Postal Code requires a minimum of 2 and a maximum of 10 characters")
	private String postalCode;

    @Size(min = 2, max = 50, message ="City requires a minimum of 2 and a maximum of 50 characters")
	private String city;

    @Size(min = 4, max = 75, message ="Country requires a minimum of 4 and a maximum of 75 characters")
	private String country;

	public Address() {
	}

	public Address(String street, String streetNumber, String postalCode, String city, String country) {
		this.street = street;
		this.streetNumber = streetNumber;
		this.postalCode = postalCode;
		this.city = city;
		this.country = country;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}


}
