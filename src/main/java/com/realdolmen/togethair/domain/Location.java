package com.realdolmen.togethair.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Location {

	@Id
	@GeneratedValue
	private Long id;

	private String name;
	private String country;
	private String airportCode;
	private String globalRegion;

	public Location() {
	}

	public Location(String name, String country, String airportCode, String globalRegion) {
		this.name = name;
		this.country = country;
		this.airportCode = airportCode;
		this.globalRegion = globalRegion;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getGlobalRegion() {
		return globalRegion;
	}

	public void setGlobalRegion(String globalRegion) {
		this.globalRegion = globalRegion;
	}

	public Long getId() {
		return id;
	}

	public String toString() {
		return airportCode+ " :  " +name ;
	}


}
