package com.realdolmen.togethair.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Flight {

    @Id
    @GeneratedValue
    private Long id;

    private Date departureDate;

    private Date arrivalTime;

    private String duration;

    @ManyToOne
    @JoinColumn(name = "departure_fk")
    private Location departureLocation;

    @ManyToOne
    @JoinColumn(name = "arrival_fk")
    private Location arrivalLocation;

    @OneToMany(mappedBy = "flight", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Ticket> tickets = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "airline_fk")
    private Airline airline;


    public int getNumberOfBusinessClassSeats() {
        return numberOfBusinessClassSeats;
    }

    public void setNumberOfBusinessClassSeats(int numberOfBusinessClassSeats) {
        this.numberOfBusinessClassSeats = numberOfBusinessClassSeats;
    }

    private int numberOfBusinessClassSeats;
    private Integer numberOfEconomyClassSeats;

    private Double baseRate;
    //private boolean discountGivenByAirline;
    //private Integer fromHowManySeatDoesAirlineGiveDiscount;
    //private Integer howMuchDiscountDoesAirlineGiveInPercentage;
    /*private int discountGivenFromAmountOfSeats;
    public int discountGivenByAmountOfSeats(int discountGivenFromAmountOfSeats) {
        if (discountGivenByAirline == true) {
            Scanner s = new Scanner;
            discountGivenFromAmountOfSeats = s.nextInt();
        }*/
    //private boolean discountGivenByEmployee;
    //private boolean flightValidatedByEmployee;



    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Integer getNumberOfEconomyClassSeats() {
        return numberOfEconomyClassSeats;
    }

    public void setNumberOfEconomyClassSeats(Integer numberOfEconomyClassSeats) {
        this.numberOfEconomyClassSeats = numberOfEconomyClassSeats;
    }

    public Double getBaseRate() {
        return baseRate;
    }

    public void setBaseRate(Double baseRateOfEconomyClassSeat) {
        this.baseRate = baseRateOfEconomyClassSeat;
    }


    //public Location getDepartureLocation() {
    //	return departureLocation;
    //}
    //public void setDepartureLocation(Location departureLocation) {
    //	this.departureLocation = departureLocation;
    //}
    //
    //public Location getArrivalLocation() {
    //	return arrivalLocation;
    //}
    //public void setArrivalLocation(Location arrivalLocation) {
    //	this.arrivalLocation = arrivalLocation;
    //}
    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flight flight = (Flight) o;
        return id == flight.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Location getDepartureLocation() {
        return departureLocation;
    }

    public void setDepartureLocation(Location departureLocation) {
        this.departureLocation = departureLocation;
    }

    public Location getArrivalLocation() {
        return arrivalLocation;
    }

    public void setArrivalLocation(Location arrivalLocation) {
        this.arrivalLocation = arrivalLocation;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public Airline getAirline() {
        return airline;
    }
    
    public void setAirline(Airline airline) {
        this.airline = airline;
    }

    public Flight(){};
    public Flight(Integer numberOfEconomyClassSeats, Integer numberOfBusinessClassSeats, String duration, Double baseRate, Date departureDate) {
        this.numberOfEconomyClassSeats = numberOfEconomyClassSeats;
        this.numberOfBusinessClassSeats = numberOfBusinessClassSeats;
        this.duration = duration;
        this.baseRate = baseRate;
        this.departureDate = departureDate;
    }
}
