package com.realdolmen.togethair;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.junit.*;


public abstract class AbstractPersistenceTest {
	
protected static EntityManagerFactory entityManagerFactory;
protected static EntityManager entityManager;

@BeforeClass
public static void initializeEntityManagerFactory() {
	entityManagerFactory = Persistence.createEntityManagerFactory("TogethAirPersistenceUnit");
}
@Before
public void initializeEntityManagerAndStartTransaction() {
	entityManager = entityManagerFactory.createEntityManager();
	entityManager.getTransaction().begin();
}

@After
public void endTransaction() {
	if(entityManager!=null) {
	entityManager.getTransaction().rollback();
	entityManager.close();}
}


@AfterClass
public static void closeEntityManagerFactory() {
	if(entityManagerFactory!=null) entityManagerFactory.close();
	
}

	protected <T> long count(Class<T> entityClass) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> query = builder.createQuery(Long.class);
		query.select(builder.count(query.from(entityClass)));
		return entityManager.createQuery(query).getSingleResult();
	}

}